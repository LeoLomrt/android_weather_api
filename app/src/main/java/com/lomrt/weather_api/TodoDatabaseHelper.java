package com.lomrt.weather_api;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TodoDatabaseHelper extends SQLiteOpenHelper
{
    private final static int DATABASE_VERSION = 1;
    private final static String DATABASE_NAME = "SampleList.db";
    private final static String TABLE_TODO = "Todo";

    public TodoDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        final String SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_TODO + "( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "day VARCHAR(50), " +
                "time VARCHAR(50), " +
                "title VARCHAR(255), " +
                "content VARCHAR(255)" +
                ");";
        db.execSQL(SQL);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        final String SQL = "DROP TABLE " + TABLE_TODO;
        db.execSQL(SQL);
    }
}
