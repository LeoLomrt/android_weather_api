package com.lomrt.weather_api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LMTMainActivity extends Activity
{
	private TodoDatabaseHelper mTodoDatabaseHelper = null;
	private ArrayList<HashMap<String, String>> mDataArrayList = new ArrayList();

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lmtmain);
		openDB();
	}

	public void getAll()
	{
		ArrayList<HashMap<String, String>> dataArrayList = new ArrayList();
		SQLiteDatabase db = mTodoDatabaseHelper.getWritableDatabase();
		String[] columns = { "id", "day", "time", "title", "content" };

		Cursor cursor = db.query("Todo", columns, null, null, null, null, null, null);

		while (cursor.moveToNext())
		{
			HashMap<String, String> dataHashMap = new HashMap();
			dataHashMap.put("content", cursor.getString(4));
			dataHashMap.put("title", cursor.getString(3));
			dataHashMap.put("time", cursor.getString(2));
			dataHashMap.put("day", cursor.getString(1));
			dataHashMap.put("id", cursor.getString(0));
			System.err.println(dataHashMap);
			dataArrayList.add(dataHashMap);
			dataArrayList.get(0).get("day");
		}

		cursor.close();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

	}

	private void insertData(String day, String time, String title, String content)
	{
		SQLiteDatabase db = mTodoDatabaseHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("title", title);
		values.put("content", content);
		values.put("day", day);
		values.put("time", time);
		db.insert("Todo", null, values);
	}

	private void openDB()
	{
		mTodoDatabaseHelper = new TodoDatabaseHelper(this);

	}

	private void closeDB()
	{
		mTodoDatabaseHelper.close();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		closeDB();
	}

	public void onInsertButtonPressed(View view)
	{
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatDay = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
		insertData(formatDay.format(calendar.getTime()), formatTime.format(calendar.getTime()), "titleAAA", "contentAAA");
		// getAll();
		// System.out.println(getAll());
	}

	public void onGetListButtonPressed(View view)
	{
		getAll();
		// System.out.println(mDataArrayList);
	}

	public void onDeleteTableButtonPressed(View view)
	{
		SQLiteDatabase db = mTodoDatabaseHelper.getWritableDatabase();
		db.delete("Todo", null, null);
		getAll();
		// System.out.println(mDataArrayList);
	}

	public void onCalendarButtonPressed(View view)
	{
		Intent intent = new Intent();
		intent.setClass(LMTMainActivity.this, LMTCalendarActivity.class);
		startActivity(intent);
	}

}
