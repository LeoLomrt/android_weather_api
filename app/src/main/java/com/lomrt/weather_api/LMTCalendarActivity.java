package com.lomrt.weather_api;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;
import android.widget.Toast;

public class LMTCalendarActivity extends AppCompatActivity
{

	private CalendarView mCalendarView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lmtcalendar);
		mCalendarView = (CalendarView) findViewById(R.id.calendarView);

		mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
		{
			public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth)
			{
				// Month從0算起
				String date = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
                Log.e("SELECT DATE",date);
			}
		});
	}
}
